import React, {  useEffect, useState } from 'react'
import { v4 } from 'uuid'
import { randomColor } from 'randomcolor'
import Draggable from 'react-draggable'
import './App.css';

function App() {
  const [item, setItem] = useState('');
  const [items, setItems] = useState(
    JSON.parse(localStorage.getItem('items')) || []
  );

  useEffect(() => {
    localStorage.setItem('items', JSON.stringify(items))
  }, [items])

  const newItem = () => {
    if (item.trim() !== '') {
      const newItem = {
        id: v4(),
        item: item,
        color: randomColor({
          luminosity: 'light'
        }),
        defaultPosition: {
          x: 500,
          y: -500
        }
      }
      setItems((items) => [...items, newItem])
      setItem('')
    } else {
      alert("Do not during, please!")
      setItem('')
    }
  }

  const deleteTask = (id) => {
    setItems([...items].filter((item) => item.id !== id))
  }

  const updatePosition = (data, index) => {
    let newArray = [...items]
    newArray[index].defaultPosition = {x: data.x, y: data.y}
    setItems(newArray)
  }

  const KeyDown = (e) => {
    const code = e.keyCode || e.which
    if (code == 13) {
      newItem()
    }
  }

  return (
    <div className="App">
      <div className='wrapper'>
        <input 
          value={item}
          type='text'
          placeholder='Input Your Todo...'
          onChange={(e) => setItem(e.target.value)}
          onKeyPress={(e) => KeyDown(e)}
        />
        <button className='enter' onClick={newItem}>ENTER</button>
      </div>

      {
        items.map((item, index) => {
          return (
            <Draggable
            onStop={(e, data) => {
              updatePosition(data, index)
            }}
            key={index}
            defaultPosition={item.defaultPosition}
            >
              <div className='todo-item' style={{backgroundColor:item.color}}>
                {`${item.item}`}
                <button 
                  onClick={() => deleteTask(item.id)}
                  className='delete'
                >
                  X
                </button>
              </div>
            </Draggable>
          )
        })
      }
    </div>
  );
}

export default App;
